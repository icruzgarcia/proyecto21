package boletín21;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;
import java.util.Scanner;

public class Methods {

    public static void readFiles(File file) {//Este metodo tan solo lee los ficheros
        Scanner ffile = null;
        ArrayList<Alumnos> go = new ArrayList<Alumnos>();
        try {
            ffile = new Scanner(file);
            while (ffile.hasNext()) {
                String cadena = ffile.nextLine();
                String[] datos = cadena.split(",");
                Alumnos mate = new Alumnos(datos[0], datos[1], Integer.parseInt(datos[2]));
                go.add(mate);
            }
        } catch (FileNotFoundException e) {
            System.out.println("No se encuentra el archivo" + e.getMessage());
        } finally {
            if (ffile != null) {
                ffile.close();
            }
        }
        for (int i = 0; i < go.size(); i++) {
            System.out.println(go.get(i));
        }

    }
    
    
    public static void pass(File students,File pass){
      Scanner ppass = null;
      PrintWriter write=null;
        ArrayList<Alumnos> go = new ArrayList<Alumnos>();
        try {
            ppass = new Scanner(students);
            write= new PrintWriter(pass);
            
            while (ppass.hasNext()) {
                String cadena = ppass.nextLine();
                String[] datos = cadena.split(",");
                 if (Integer.parseInt(datos[2])>=5){
                     write.println(new Alumnos(datos[0],datos[1],Integer.parseInt(datos[2])));
                 }
            }
        } catch (FileNotFoundException e) {
            System.out.println("No se encuentra el archivo" + e.getMessage());
        } finally {
            if (ppass != null) {
                ppass.close();
                write.close();
            }
        }  
    }
    
    public static void quest (File students){
         Scanner ffile = null;
        ArrayList<Alumnos> go = new ArrayList<Alumnos>();
        try {
            ffile = new Scanner(students);
            while (ffile.hasNext()) {
                String cadena = ffile.nextLine();
                String[] datos = cadena.split(",");
                Alumnos mate = new Alumnos(datos[0], datos[1], Integer.parseInt(datos[2]));
                go.add(mate);
            }
        } catch (FileNotFoundException e) {
            System.out.println("No se encuentra el archivo" + e.getMessage());
        } finally {
            if (ffile != null) {
                ffile.close();
            }
        }
        
        
    
    
    
    }
    
    public int compareTo(Object i) {
        Alumnos alumno = (Alumnos) i;
        if (alumno.getNombre().compareToIgnoreCase(alumno.getNombre()) == 0) {
            return 0;
        } else if (alumno.getNombre().compareToIgnoreCase(alumno.getNombre()) < 0) {
            return -1;
        } else {
            return 1;
        }

    }
}
