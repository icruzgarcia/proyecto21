package boletín21;

public class Alumnos {

    private String nombre, codigo;
    private int nota;

    public Alumnos() {
    }

    public Alumnos(String nombre, String codigo, int nota) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.nota = nota;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getNota() {
        return nota;
    }

    public void setNota(int nota) {
        this.nota = nota;
    }

    public String toString() {
        return ("Nombre: " + this.nombre + "\nNota: " + this.nota);
    }

}
