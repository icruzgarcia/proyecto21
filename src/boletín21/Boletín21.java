
package boletín21;

import java.io.File;

/**
 * 1- Crea un fichero llamado "datosAlumnos.txt" con String nombre, String codigo e int nota y generar un fichero a partir de este con los aprobados unicamente, "aprobados.txt"
 * 2- Consultar la nota de un alumno,tomando,como campo clave el codigo
 * 3- Añadir alumnos (hasta que su código sea FIN o algo)
 *
 * @author icruzgarcia
 */
public class Boletín21 {

    public static void main(String[] args) {
        File stundent= new File("datosAlumnos.txt");
        File pass=new File("aprobados.txt");
        Methods.readFiles(stundent);
        Methods.pass(stundent, pass);
        
    }
    
}
